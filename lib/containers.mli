(*********************************************************************************)
(*                OCaml-LDP                                                      *)
(*                                                                               *)
(*    Copyright (C) 2016-2024 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Handling containers. *)

(** A tree of containers. *)
type tree =
| NRDF of Iri.t * Ct.t (** A non-RDF resource with its IRI and content-type *)
| RDF of Iri.t (** A RDF resource with its IRI *)
| CONT of Iri.t * tree list (** A container with its IRI and children. *)

(** Returns a string representation for the give {!tree} node (not
  recursive). *)
val node_text : tree -> string

(** Result of the {!Make} functor. *)
module type S = sig
    (** [containers iri] recursively gets the contents of the given IRI to
       build a tree of containers. *)
    val containers : Iri.t -> tree Lwt.t
  end

(** [Make(Http)] returns a {!S} using the given Http implementation to
  perform operations. *)
module Make :
  functor (P : Http.Http) -> S
