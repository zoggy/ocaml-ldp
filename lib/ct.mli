(*********************************************************************************)
(*                OCaml-LDP                                                      *)
(*                                                                               *)
(*    Copyright (C) 2016-2024 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Simple content-type parsing/printing.
  Tries to implement {{:https://tools.ietf.org/html/rfc2045}RFC 2045}. *)

(** {2 Base types} *)

type token = string (** Non-empty string containing only
     ascii CHAR except SPACE, CTLs, or tspecials as
     defined {{:https://www.w3.org/Protocols/rfc1341/4_Content-Type.html}here}. *)

type ty =
  | Application
  | Audio
  | Ietf of token
  | Image
  | Message
  | Multipart
  | Text
  | Video
  | X of token

type subty = token
type value = T of token | Quoted of string
type parameter = token * value

type error = string * Lexing.position
val string_of_error : error -> string

(** {2 Content-types} *)

type t = { ty : ty ; subty : subty ; parameters : parameter list }

(** [create ?paramers ty subty] creates a new content-type. *)
val create : ?parameters:parameter list -> ty -> subty -> t

(** Default content-type is [text/plain] with [charset] parameter set to ["us-ascii"].*)
val default : t

val of_string : string -> (t, error) result
val to_string : t -> string

(** [value p t] returns value associated to parameter [p] in [t], if any. *)
val value : token -> t -> string option

(** [value p def t] returns value associated to [p] in [t], or [def] if
  there is no parameter [p] in [t]. *)
val value_def : token -> string -> t -> string

(** Same as {!val-value} but for parameter ["charset"]. *)
val charset : t -> string option

(** Same as {!value_def} but for paramter ["charset"]. *)
val charset_def : string -> t -> string

(** {2 Mime-types} *)

type mime = ty * subty
val to_mime : t -> mime
val of_mime : mime -> t

val mime_to_string : mime -> string
val mime_of_string : string -> (mime, error) result

(** [has_mime ct mime] returns [true] is type and subtype of [mime] are the
  same as in [ct], else [false]. *)
val has_mime : t -> mime -> bool

(** {2 Predefined content-types} *)

val ct_turtle : t (** [text/turtle] *)

val ct_xmlrdf : t  (** [application/rdf+xml] *)

val ct_sparql_update : t  (** [application/sparql-update] *)

val ct_text : t  (** [text/plain] *)

val ct_xhtml : t (** [application/xhtml+xml] *)

(** {2 Predefined mime-types} *)

val mime_turtle : mime (** [text/turtle] *)

val mime_xmlrdf : mime (** [application/rdf+xml] *)

val mime_sparql_update : mime (** [application/sparql-update] *)

val mime_text : mime (** [text/plain] *)

val mime_xhtml : mime (** [application/xhtml+xml] *)

