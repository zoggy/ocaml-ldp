(*********************************************************************************)
(*                OCaml-LDP                                                      *)
(*                                                                               *)
(*    Copyright (C) 2016-2024 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Base types and utilities. *)

open Cohttp

(** {2 Resources} *)

(** Information about a resource. *)
type meta =
  { iri : Iri.t (** IRI of the resource *);
    acl : Iri.t option (** Optional IRI of the ACL (access control list) of the resource. *) ;
    meta: Iri.t option (** Optional IRI of meta information about the resource. *) ;
    user: string option (** Optional used identifier when resource was retrieved.*) ;
    websocket: Iri.t option (** Optional IRI of a websocket tunnel for the resource.*) ;
    editable : Code.meth list (** Allowed HTTP methods on the resource.*) ;
    exists: bool (** Whether the resource exists. *) ;
    info: Response.t * Cohttp_lwt.Body.t (** Original HTTP response and body. *) ;
  }

(** [meta resp body] creates a {!type-meta} structure.
  Optional fields have default value [None]. Default
  [editable] is [[]]. Default [exists] is [true]. *)
val meta : ?acl:Iri.t -> ?meta:Iri.t -> ?user:string ->
  ?websocket:Iri.t -> ?editable:Code.meth list -> ?exists:bool ->
    Response.t -> Cohttp_lwt.Body.t -> Iri.t -> meta

(** Returns a string representation of a {!Types.type-meta}, mainly for debugging
  purpose; *)
val string_of_meta : meta -> string

(** A RDF resource contains meta information on the resource, its graph,
   content-type and original contents as a string. *)
type rdf_resource =
  { meta : meta ;
    graph: Rdf.Graph.graph ;
    ct: Ct.t ;
    contents: string ;
  }

(** A Non-rdf resource contains meta information on the resource, its
  content-type and its original contents as a string. *)
type non_rdf_resource =
  { meta : meta ;
    ct: Ct.t ;
    contents: string ;
  }

(** A resource may be a container, another RDF resource or a non-RDF resource. *)
type resource =
| Container of rdf_resource
| Rdf of rdf_resource
| Non_rdf of non_rdf_resource

(** [container_children g] returns the IRIs of the children of the
  given container. Container IRI is the name of the graph. Its children
  are retrieved by looking for the triples with the name of the
  graph as subjects and the {!Rdf.Ldp.contains} relation. *)
val container_children : Rdf.Graph.graph -> Iri.t list

(** {2 Errors} *)

(** The error type, with constructors added by other modules. *)
type error = ..

(** Exception used in the library to signal an error. *)
exception Error of error

(** [error e] raises a {!Error} exception with the given {!type-error}. *)
val error : error -> 'a

(** [fail e] calls {!Lwt.fail} with a {!Error} exception with the given {!type-error}. *)
val fail : error -> 'a Lwt.t

(** Returns a string representation of the given error. *)
val string_of_error : error -> string

(** [register_string_of_error f] registers f as printer for errors. [f]
  takes the previous printer function, to use it as fallback for errors
  not handled. *)
val register_string_of_error : ((error -> string) -> error -> string) -> unit

type error +=
| Invalid_method of string
| Missing_pred of Iri.t * Iri.t
| Missing_pred_iri of Iri.t * Iri.t
| Request_error of Iri.t * string (* with an exception ?? *)
| Parse_error of Iri.t * exn
| Unsupported_format of Iri.t * Ct.mime
| Other of exn

(** {2 Misc} *)

(** [content_type_of_string str] parses [str] as content-type, using
  {!Ldp.Ct.of_string}. If parsing fails and [fail] is [true] (which is the default),
  raises [Failure] with the error message, else return the default content-type
  {!Ldp.Ct.default}. *)
val content_type_of_string : ?fail:bool -> string -> Ct.t

(** [split_string string chars] splits the given [string] on any of the
  [chars]. Optional argument [keep_empty] (default is [false]) indicates
  whether empty strings must be returned too. *)
val split_string : ?keep_empty:bool -> string -> char list -> string list

(** [methods_of_string str] returns the list of HTTP methods from the
  given string [str], where they may be separated by characters [','], [' ']
  or ['\t']. *)
val methods_of_string : string -> Code.meth list
