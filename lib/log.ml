(*********************************************************************************)
(*                OCaml-LDP                                                      *)
(*                                                                               *)
(*    Copyright (C) 2016-2024 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** *)

let src = Logs.Src.create ~doc:"log of the Ldp library" "ldp"

let err f = Logs.err ~src f
let warn f = Logs.warn ~src f
let info f = Logs.info ~src f
let debug f = Logs.debug ~src f

let err_lwt f = Logs_lwt.err ~src f
let warn_lwt f = Logs_lwt.warn ~src f
let info_lwt f = Logs_lwt.info ~src f
let debug_lwt f = Logs_lwt.debug ~src f

let bad_log_level s = Printf.sprintf "Bad log level value: %S" s

let level_of_string s =
  let open Logs in
  match String.lowercase_ascii s with
    "app" | "0" -> Logs.App
  | "error" | "1" -> Error
  | "warning" | "2" -> Warning
  | "info" | "3" -> Info
  | "debug" | "4" -> Debug
  | _ -> failwith (bad_log_level s)

let string_of_level = function
  | Logs.App -> "app"
  | Error -> "error"
  | Warning -> "warning"
  | Info -> "info"
  | Debug -> "debug"

let level_wrapper =
  let to_json ?with_doc l = `String (string_of_level l) in
  let from_json ?(def=Logs.App) = function
  | `String s -> 
      (try level_of_string s
       with Failure msg -> err (fun m -> m "%s" msg); def
      )
  | json -> 
      err (fun m -> m "%s" (bad_log_level (Yojson.Safe.to_string json)));
      def
  in
  let w = Ocf.Wrapper.make to_json from_json in
  Ocf.Wrapper.option w

let log_level = Ocf.option
  ~cb: (Logs.Src.set_level src)
  level_wrapper None
