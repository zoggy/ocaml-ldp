(*********************************************************************************)
(*                OCaml-LDP                                                      *)
(*                                                                               *)
(*    Copyright (C) 2016-2024 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** *)

module S = Cohttp_lwt_unix.Server

let reform_pem str =
  match Server_misc.split_string str [' '] with
  | beg::cert::q ->
      begin
        match List.rev q with
          cert2 :: en :: q ->
            let l = (beg ^ " " ^cert) ::
              (List.rev q) @
                [en ^ " " ^cert2]
            in
            String.concat "\n" l
        | _ -> str
      end
  | _ -> str

let server conf http_handler =
  let open Server_conf in
  let callback _conn req body =
    let headers = Cohttp.Request.headers req in
    let%lwt user =
      match Cohttp.Header.get headers conf.client_cert_header with
        None ->
          let%lwt () = Server_log._debug_lwt
            (fun m -> m "No %s header" conf.client_cert_header)
          in
          Lwt.return_none
      | Some str ->
          let str = reform_pem str in
          let%lwt () = Server_log._debug_lwt (fun m -> m "PEM: %s" str) in
          match X509.Certificate.decode_pem str with
          | Ok c -> Server_auth.user_of_cert c
          | Error (`Msg msg) ->
              Server_log._debug
                (fun m -> m "Error while reading client PEM: %s\n%s"
                   msg (Cohttp.Header.to_string headers));
              Lwt.return_none
    in
    http_handler ?user req body
  in
  let conn_closed (_,id) = () in
  let config = S.make ~callback ~conn_closed () in
  let%lwt ctx = Conduit_lwt_unix.init ~src:conf.host() in
  let ctx = Cohttp_lwt_unix.Net.init ~ctx () in
  let mode = `TCP (`Port conf.port) in
  S.create ~ctx ~mode config
