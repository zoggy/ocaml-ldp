{0 Solid_server}

The [solid_server] package provides an experimental
{{:https://solidproject.org}SOLID} server, with
the [solid-server] executable.

{1 Features}

By now, the server implements authentication only based on
{{:https://www.w3.org/2005/Incubator/webid/spec/tls/}WebId-TLS}, i.e.
user's authentication is based on the certificate provided in the
TLS connection.

The server implements {{:https://solidproject.org/TR/wac}Solid-WAC}
for access control on resources.

The server can serve files stored in regular file hierarchy
or in a git repository. In the latter case, the server can serve
any revision of a file or the head of any branch.

{1 Configuration}

You can find various examples of configuration files (suffixed by [.json]) in the
directories of the {{:https://framagit.org/zoggy/ocaml-ldp/-/tree/master/tests}tests}
directory of the repository.

Configuration files are in JSON format, with root value being a JSON
object of th following form:

{@json[
{
  "storage": { ... },
  "log": { ... } ,
  "ldp": { ... },
  "https": { ... },
  "http": { ... },
}
]}

{2 Storage}

The [storage] field describe the mapping between urls and files. Here is an example
to show the various possibilities.

{code@json[ storage: {
    "root": "www", <-- root directory to store served documents
    "rules": [ <-- the rules to map urls to files; the first rule matching is applied
      { host: "^([^.]+).foobar.net", root: "home/{1}" }, <-- a rule
      // "gee.Foobar.net/toto/tutu.html "will be mapped to w"ww/home/gee/toto/tutu.html".
      // {1} will be replaced by the first regular expression matched (here in host).

      { path: "^/~([a-zA-Z][a-zA-Z0-9]*)", root: "home/{1}" },
      // If previous rule did not apply, path "/~toto/tutu.html" will be mapped to
      // "www/home/toto/tutu.html".
      // {1} will be replaced by the first regular expression matched (here in path).
      // When a regular expression matches in path, the matched string is removed
      // from the path, to that the rest of the path is append to the result
      // of the "root" field.

      { root: "documents", read_only:true },
      // Path "/documents/toto/tutu.html" will be mapped to file "www/documents/toto/tutu.html"
      // in read-only mode, i.e. only HEAD and GET methods will be allowed in HTTP queries.

      { path: "/v/", root: "repo/", fs_type: "git" },
      // Urls with path beginning by "/v/" will be mapped to the git repository in
      // "www/repo". For example:
      // - "/v/master/tutu/toto.html" will be mapped to file "www/repo/utu/toto.html" at the
      //   head of master branch.
      // - "/v/<commitid>/tutu/toto.html" will be mapped to file "www/repo/utu/toto.html" in
      //   revision <commitid>
      // - "/v/<tag>/tutu/toto.html" will be mapped to file "www/repo/utu/toto.html" in
      //   tag <tag>.

      { path: "/tmp/", root: "/tmp/t/", fs_type: "git", options: { bare: true} }
      // Urls with path beginning by "/tmp/" will be mapped to the bare git repository in
      // "/tmp/t", the same way as above, for example "/tmp/master/tutu/toto.html"
      // will be mapped to file "/tmp/t/tutu/toto.html" at the head of master branch.
    ]
  }
]code}

Web ACL are retrieved from the [,acl] files, as specified in solid WAC specification.
For git repositories, the [,acl] file of a file is always retrieved from the head of the
master branch.

{2 Log}

The [log] field specifies log levels. For example:

{@json[
 ...
 "log": { "server": "debug", "library": info, "global": "warning" },
 ...
]}
will set all log level of all log sources (see {!Logs.Src}
to "warning", except for library ({!Ldp.Log}) which is set to "info"
and "server" which is set to "debug".

{2 Ldp}

The [ldp] field specifies some behaviours relative to the way resources are served.
By now, only the field [container_listing] is supported, which allows to specify
how the contents of containers are served when queried by GET method.
The [null] value will make the server return a 415 HTTP error. If a list
of filenames is provided, each name is looked up for relatively to the container url
and the first one which exists is served. If none of the given filenames is found,
the a default XHTML page is created, listing the resources the client can read.

Example:

{@json[
  ...
  "ldp": { "container_listing": [ "index.html", "index.xhtml" ] },
  ...
]}

{2 https}

The [https] field specifies the port the server listens to, and the optional
SSL certificate and key files. Example:

{@json[
  "https": {
    "port": 9999,
    "key_file": "server-certificates/server.key",
    "cert_file": "server-certificates/server.pem"
  }
]}

{2 http}

The [http] header field specifies how to listen to HTTP queries without TLS.
If [https] field is specified, the server will not serve HTTP queries without TLS.
The port to listen to can be specified. The server can respond to queries
only for a given hostname. Setting hostname to [0.0.0.0] will make the server
respond to all queries.

The [client_cert_header] field is used to specify the argument where client
certificate can be found. Default is [X-SSL-Cert]. This is useful when the
server is behind an apache proxy which will map TLS connection to local simple
HTTP connection and embed the client certificate into this argument in queries.

Example:

{@json[
  "http": {
    port: 9999,
    hostname: "localhost",
    client_cert_header: "X-SSL-Cert"
  }
]}

{1 Adding user and profile}

The [solid-server] executable cal also be used to create a new user
in the file hierarchy of a given configuration. It can create
a client's certificate and key files and her first workspaces.

The following example creates a new user [https://localhost:9999/tutu]
in the server configuration from file
[config.json], with a given name, creating a certificate in files [tutu.pem]
and [tutu.key] (with option [--create-cert]), and creating also a profile
card (option [--profile]):

{@sh[
solid-server -c config.json --log-level "" \
  --add-user https://localhost:9999/tutu \
  --name "Tutu Turlututu" \
  --cert-label "Tutu's self-signed cert" \
  --create-cert tutu \
  --profile
]}


