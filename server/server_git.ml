(*********************************************************************************)
(*                OCaml-LDP                                                      *)
(*                                                                               *)
(*    Copyright (C) 2016-2024 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

open Lwt.Infix
open Server_fs
open Git_unix
(*open Git_unix.Value_IO*)
module Store = Git_unix.Make(Digestif.SHA1)
module Search = Git.Search.Make(Digestif.SHA1)(Store)
(*module Value = Git_unix.Store.Value*)
module Value = Store.Value
module Commit = Value.Commit
module Hash = Git_unix.Store.Hash
module Tree = Value.Tree

type path = { path : Server_fs.path ; commit : Commit.t }

let str_opt = function None -> "" | Some s -> s
let map_opt f def = function None -> def | Some x -> f x

type Ldp.Types.error +=
| Missing_git_reference of Iri.t * string list
| Invalid_git_reference of Iri.t * string
| File_not_found of string list * Hash.t option
| Invalid_git_path of string list  * Hash.t option

let () = Ldp.Types.register_string_of_error
  (fun fb -> function
     | Missing_git_reference (iri, rel) ->
         Printf.sprintf "Missing git reference: %s (%s)"
           (String.concat "/" rel) (Iri.to_string iri)
     | Invalid_git_reference (iri, str) ->
         Printf.sprintf "Invalid git reference: %s (%s)"
           str (Iri.to_string iri)
     | File_not_found (file, hash) ->
         Printf.sprintf "File not found: %s [%s]"
           (String.concat "/" file)
           (map_opt Hash.to_hex "" hash)
     | Invalid_git_path (file, hash) ->
         Printf.sprintf "File not found: %s [%s]"
           (String.concat "/" file)
           (map_opt Hash.to_hex "" hash)
     | e -> fb e
  )

module Wm = Server_webmachine.Wm

let git_read git file hash =
  let%lwt () = Server_log._debug_lwt
    (fun m -> m "git_read %S" (String.concat "|" file))
  in
  match%lwt Search.find git hash (`Commit (`Path file)) with
  | None -> Ldp.Types.fail (File_not_found (file, Some hash))
  | Some sha -> Store.read_exn git sha >>= function
      | Git.Value.Blob b -> Lwt.return (`Blob (Value.Blob.to_string b))
      | Tree t -> Lwt.return (`Tree t)
      | _ -> Ldp.Types.fail (Invalid_git_path (file, Some hash))
 ;;

module type Ref = sig
    val git : Store.t
    val hash : Hash.t
  end

module type Options =
  sig
    include Server_fs.Options
    val bare : bool
  end
module Make (O:Options) (R:Ref) : Server_fs.P =
  struct
    let to_filename root_dir = function
    | "tags" :: _ :: q
    | _ :: q -> String.concat Filename.dir_sep q
    | _ -> ""

    let to_file = function
    | "" -> Lwt.return []
    | s -> Lwt.return (Server_misc.split_string s ['/'])

    let file_exists file =
      let%lwt file = to_file file in
      match%lwt Search.find R.git R.hash (`Commit (`Path file)) with
      | None   -> Lwt.return_false
      | Some _ -> Lwt.return_true

    let is_file file_ =
      let%lwt file = to_file file_ in
      match%lwt Search.find R.git R.hash (`Commit (`Path file)) with
      | None ->
          let%lwt () = Server_log._debug_lwt
            (fun m -> m "is_file %S [%s]: None" (file_)
             (Hash.to_hex R.hash))
          in
          Lwt.return_none
      | Some sha -> Store.read_exn R.git sha >>= function
          | Git.Value.Blob b -> Lwt.return_some true
          | _ -> Lwt.return_some false

    let string_of_file ?(on_err=fun _ -> Lwt.return "") file =
      let%lwt file = to_file file in
      match%lwt git_read R.git file R.hash with
      | `Blob str -> Lwt.return str
      | _ -> on_err (Ldp.Types.Error (File_not_found (file, Some R.hash)))
      | exception e -> on_err e

    let string_opt_of_file ?(on_err=fun _ -> Lwt.return_none) file =
      let%lwt file = to_file file in
      match%lwt git_read R.git file R.hash with
      | `Blob str -> Lwt.return_some str
      | _ -> on_err (Ldp.Types.Error (File_not_found (file, Some R.hash)))
      | exception e -> on_err e

    let stat_kind file =
      let%lwt file = to_file file in
      match%lwt Search.find R.git R.hash (`Commit (`Path file)) with
      | None -> Ldp.Types.fail (File_not_found (file, Some R.hash))
      | Some sha -> Store.read_exn R.git sha >>= function
          | Git.Value.Blob b -> Lwt.return Unix.S_REG
          | Tree t -> Lwt.return Unix.S_DIR
          | _ -> Ldp.Types.fail (Invalid_git_path (file, Some R.hash))

    let stat_mtime file =
      match%lwt Store.read R.git R.hash with
      | Ok (Git.Value.Commit c) ->
          let a = Commit.author c in
          let (d, _) = a.Git.User.date in
          let%lwt () = Server_log._debug_lwt
            (fun m -> m "commit date for %S: %s" file (Int64.to_string d))
          in
          Lwt.return (Int64.to_float d)
      | _ ->
          let%lwt () = Server_log._debug_lwt
            (fun m -> m "no mtime found for %S" file)
          in
          Lwt.return 0.0

    let dir_entries dir =
      let%lwt dir = to_file dir in
      match%lwt git_read R.git dir R.hash with
      | `Tree t ->
          let names = List.map (fun e -> e.Git.Tree.name) (Tree.to_list t) in
          Lwt.return (Lwt_stream.of_list names)
      | _ -> Lwt.return (Lwt_stream.of_list [])

    let store_graph _ = assert false
    let store_graph _ = assert false
    let mkdir _ = assert false
    let safe_unlink _ = assert false
    let unlink _ = assert false
    let rmdir _ = assert false
    let try_write_file _ = assert false
  end

let rec hash_of_ref git r =
  match%lwt Store.Ref.read git r with
  | Error e -> Lwt.fail_with (Fmt.to_to_string Store.pp_error e)
  | Ok (Git.Reference.Ref r) -> hash_of_ref git r
  | Ok (Git.Reference.Uid hash) ->
      (* if it is a tag object, get the object hash *)
      match%lwt Store.read git hash with
      | Ok (Git.Value.Tag t) -> Lwt.return (Value.Tag.obj t)
      | _ -> Lwt.return hash

let git_fun ~root_iri ~(root_dir:string) ~rel ~options =
  let root_dir = Fpath.v root_dir in
  let o = Server_fs.options_of_json options in
  let bare = Ocf.bool ~doc: "bare git repository" false in
  let g = Ocf.add Ocf.group ["bare"] bare in
  ignore(Ocf.from_json g options);
  let module O =
  struct
    include (val o)
    let read_only = true
    let bare = Ocf.get bare
  end
  in
  let%lwt git =
    let dotgit = if O.bare then Some root_dir else None in
    match%lwt Store.v ?dotgit root_dir with
    | Ok git -> Lwt.return git
    | Error e -> Lwt.fail_with (Fmt.to_to_string Store.pp_error e)
  in
  let%lwt hash =
    match rel with
    | [] -> Ldp.Types.fail (Missing_git_reference (root_iri, rel))
    | "tags" :: tag :: _ ->
        begin
          try%lwt
            match Git.Reference.of_string ("refs/tags/"^tag) with
            | Ok r -> hash_of_ref git r
            | Error (`Msg _msg) ->
                Ldp.Types.fail (Invalid_git_reference (root_iri, tag))
          with _ -> Ldp.Types.fail (Invalid_git_reference (root_iri, tag))
        end
    | r :: rel ->
        try%lwt 
          match Git.Reference.of_string ("refs/heads/"^r) with
          | Ok r -> hash_of_ref git r
          | Error (`Msg _msg) -> 
              Ldp.Types.fail (Invalid_git_reference (root_iri, r))
        with _ ->
            try Lwt.return (Hash.of_hex r)
            with exn -> Ldp.Types.fail (Invalid_git_reference (root_iri, r))
  in
  let%lwt master = hash_of_ref git Git.Reference.master in
  let module R = Make (O) (struct let git = git let hash = hash end) in
  let module Fs = Server_fs.Make_fs (R) (O) in
  let module R_acl = Make (O) (struct let git = git let hash = master end) in
  let module Fs_acl = Server_fs.Make_fs (R_acl) (O) in
  Server_fs.make_t (module O) (module Fs) (module Fs_acl)

let () = Server_fs.register_fs_type "git" git_fun
