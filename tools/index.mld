{0 Solid_tools}

This package provides the {!Solid_tools.Common} module
to create programs interacting with SOLID pods, with
predefined command line options. The package comes with
{{!section-tools}simple tools}.

See the various [solid_tool_*.ml] files in
{{:https://framagit.org/zoggy/ocaml-ldp/-/tree/master/tools}the repository}.

These tools allow to choose (with [-p] option) the profile to
use when connecting to SOLID pod.
The available profiles are defined in [~/.solid/profiles.json], which looks like this:

{@json[
[
  { id: "me",
    cert: ("zoggy.pem", "zoggy.key"),
    cache: "solid-cache"
  }
]
]}

It defines a list of profiles (here, only one with id ["me"]). Each profile
can indicate certificate and key files (in the [cert] field) and whether
to use a cache (with a directory name in the [cache] field). Relative
filenames are relative to the [~/.solid] directory.

{1:tools Tools}

The following tools comes with the [solid_tools] package. They all share common
options to choose user profile, specifying certificates, using cache or not, ...
and some have additional options. Use the [--help] option to see usage message.

{2:solid_ls [solid-ls]}

This tool retrieves the container and resource hierarchy from IRIs.

Example:

{@sh[$ solid-ls https://solidweb.me/ewingson/
https://solidweb.me/ewingson/
  https://solidweb.me/ewingson/site/
    https://solidweb.me/ewingson/site/spcom.png [image/png]
    https://solidweb.me/ewingson/site/index.html [text/html]
  https://solidweb.me/ewingson/profile/ [application/json]
  https://solidweb.me/ewingson/README [text/markdown]
  https://solidweb.me/ewingson/2022/ [application/json]
]}

{2:solid_get [solid-get]}

This tool retrieves a resource and prints it to standard output, file or
directory (in case of recursive queries, with [-r] option).

Example:

{@sh[$ solid-get https://solidweb.me/ewingson/
@prefix posix: <http://www.w3.org/ns/posix/stat#> .
@prefix ldp: <http://www.w3.org/ns/ldp#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix dc: <http://purl.org/dc/terms/> .
<https://solidweb.me/ewingson/site/> posix:mtime "1656890536"^^xsd:integer .
<https://solidweb.me/ewingson/site/> rdf:type ldp:Resource .
<https://solidweb.me/ewingson/site/> rdf:type ldp:Container .
<https://solidweb.me/ewingson/site/> rdf:type ldp:BasicContainer .
<https://solidweb.me/ewingson/site/> dc:modified "2022-07-03T23:22:16.000Z"^^xsd:dateTime .
<https://solidweb.me/ewingson/profile/> posix:mtime "1650525455"^^xsd:integer .
<https://solidweb.me/ewingson/profile/> rdf:type ldp:Resource .
...
]}

{2:solid_put [solid-put]}

This tool performs a PUT query on a given IRI. Body of query can be a given file.
Content-type is specified with [--mime]. A container can be created with [-c].

{2:solid_delete [solid-delete]}

This tool performs DELETE queries on the given IRIs.




