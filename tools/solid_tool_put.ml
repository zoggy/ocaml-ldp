(*********************************************************************************)
(*                OCaml-LDP                                                      *)
(*                                                                               *)
(*    Copyright (C) 2016-2024 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

let string_of_file name =
  Lwt_io.with_file ~mode: Lwt_io.Input name Lwt_io.read

let usage = Printf.sprintf
  "%s [options] <uri> [<file>]\nwhere options are:"
  Sys.argv.(0)

let mk_container = ref false
let content_type = ref Ldp.Ct.ct_turtle

let arg_ct s =
  match Ldp.Ct.of_string s with
  | Ok ct -> content_type := ct
  | Error e -> prerr_endline (Ldp.Ct.string_of_error e); exit 1

let options = [
    "--mime", Arg.String arg_ct, " set content-type" ;
    "-c", Arg.Set mk_container, " create a container" ;
  ]

let f args http =
  let module H = (val http : Ldp.Http.Http) in
  try%lwt
    match args with
    | [iri] when !mk_container ->
        let iri = Iri.of_string iri in
        let%lwt mt = H.post_container_exn iri in
        Lwt_io.write Lwt_io.stdout (Ldp.Types.string_of_meta mt)

    | iri :: file :: _ ->
        let iri = Iri.of_string iri in
        let%lwt data = string_of_file file in
        let typ =
          if Ldp.Ct.has_mime !content_type Ldp.Ct.mime_turtle then
            Rdf.Ldp.c_RDFSource
          else
            Rdf.Ldp.c_NonRDFSource
        in
        let%lwt mt = H.put_exn ~data ~ct: !content_type ~typ iri in
        Lwt_io.write Lwt_io.stdout (Ldp.Types.string_of_meta mt)

  | [] | [_] -> Lwt.fail_with usage
  with
  | Ldp.Types.Error e ->
      let msg = Ldp.Types.string_of_error e in
      Lwt_io.write_line Lwt_io.stderr msg

let () = Solid_tools.Common.main ~options ~usage f
