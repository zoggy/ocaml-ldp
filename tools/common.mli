(*********************************************************************************)
(*                OCaml-LDP                                                      *)
(*                                                                               *)
(*    Copyright (C) 2016-2024 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Common module to create SOLID tools. See examples in
  [tools/] directory of source distribution.
*)

(** [main ?options ?usage f] will parse the command line
  and call [f args Http].
  Predefined command line options allow to select a profile, use
  curl instead of cohttp+tls, use a cache directory, specify certificates, ...

  [options] optional argument can be used to specify additional command line
  options. A specific [usage] message can be specified too.

  The [f] function takes the anonymous arguments of the command line and the
  {!Ldp.Http.module-type-Http} module to use to perform queries.
*)
val main :
  ?options:(Arg.key * Arg.spec * Arg.doc) list ->
  ?usage:Arg.usage_msg ->
  (string list -> (module Ldp.Http.Http) -> unit Lwt.t) -> unit
