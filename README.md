# ocaml-ldp

[🌐 OCaml-ldp homepage](https://zoggy.frama.io/ocaml-ldp/)

Library to build [LDP](http://www.w3.org/TR/ldp/) (Linked Data Platform)
and [SOLID](https://solidproject.org/) applications,
runnable either in standalone program (using packages `ldp_tls` or `ldp_curl`)
or in the browser (using package `ldp_js` with js_of_ocaml).

Comes with a SOLID server (currently under development).

## Documentation

Documentation is available [here](https://zoggy.frama.io/ocaml-ldp/refdoc/index.html).

## Development

Development is hosted on [Framagit](https://framagit.org/zoggy/ocaml-ldp).

OCaml-ldp is released under LGPL3 license.

## Installation

The packages are installable with opam:

```sh
$ opam install ldp ldp_curl ldp_tls ldp_js
$ opam install solid solid_server solid_tools
```

Current state of OCaml-ldp can be installed with:

```sh
$ opam pin add [package] git@framagit.org:zoggy/ocaml-ldp.git
```
## Releases

See [Changes](https://framagit.org/zoggy/ocaml-ldp/-/blob/master/Changes) file for release details.

- [0.4.0](https://zoggy.frama.io/ocaml-ldp/releases/ocaml-ldp-0.4.0.tar.gz) (2024/12/11)
- [0.3.0](https://zoggy.frama.io/ocaml-ldp/releases/ocaml-ldp-0.3.0.tar.gz) (2024/10/07)
- [0.2.0](https://zoggy.frama.io/ocaml-ldp/releases/ocaml-ldp-0.2.0.tar.gz) (2024/05/28)
