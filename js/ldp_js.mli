(*********************************************************************************)
(*                OCaml-LDP                                                      *)
(*                                                                               *)
(*    Copyright (C) 2016-2024 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)


(** Performing queries using {!Cohttp_lwt_jsoo.Make_sync_client}. *)

module type P =
  sig
    val dbg : string -> unit Lwt.t
  end

module Make : P -> Ldp.Http.Requests

(** Performing HTTP queries using [Firebug.console##log] as logging function. *)
module Dbg : Ldp.Http.Requests

(** Performing HTTP queries with no debug. *)
module Nodbg : Ldp.Http.Requests

(** [login_iri ()] returns IRI of current DOM window. *)
val login_iri : unit -> Iri.t
