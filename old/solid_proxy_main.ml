open Lwt.Infix

let proxy_of_profile cert priv_key =
  let%lwt certificates =
    X509_lwt.private_of_pems ~cert ~priv_key >>=
      fun c -> Lwt.return (`Single c)
  in
  (*let conf =
    let open Server_conf in
    let g = Ocf.as_group Server_conf.https in
    Ocf.from_string g
      {| { "port": 9999,
         "key_file": "./server-certificates/localhost.key",
         "cert_file": "./server-certificates/localhost.crt"
         } |} ;
    match Ocf.get Server_conf.https with
      None -> assert false
    | Some c -> c
  in
  *)
  let conf =
    let open Server_conf in
    let g = Ocf.as_group Server_conf.http in
    Ocf.from_string g {| { "port": 9999 } |} ;
    match Ocf.get Server_conf.http with
      None -> assert false
    | Some c -> c
  in
  Solid_proxy.http_proxy conf certificates

let options = []

let usage = Printf.sprintf
  "Usage: %s [options] pemfile keyfile\nwhere options are:" Sys.argv.(0)
let main () =
  let args = ref [] in
  match Arg.parse (Arg.align options) (fun s -> args := s :: !args) usage with
  | exception Arg.Bad msg -> Lwt.fail_with msg
  | exception Ocf.Error e -> Lwt.fail_with (Ocf.string_of_error e)
  | () ->
      match List.rev !args with
      | [ pem_file ; key_file ] -> proxy_of_profile pem_file key_file
      | _ -> failwith usage


let () =
  Logs.set_reporter (Server_log.lwt_reporter ());
  try Lwt_main.run (main ())
  with
    e ->
      let msg =
        match e with
          Failure msg
        | Sys_error msg -> msg
        | Unix.Unix_error (e,s1,s2) ->
            Printf.sprintf "%s %s: %s"
              (Unix.error_message e) s2 s1
        | Ocf.Error e -> Ocf.string_of_error e
        | e ->
            Printexc.to_string e
      in
      prerr_endline msg ;
      exit 1