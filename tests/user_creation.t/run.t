Initialize variables:
  $ export URL=https://localhost:9999/
  $ export PATH=${DUNE_SOURCEROOT}/_build/default/tests:${PATH}
  $ export BUILD_PATH_PREFIX_MAP=
  $ export SOLID_SERVER_OPENSSL_REDIRECT_STDERR=1
Initialize directoties:
  $ rm -fr www
  $ cp -r origin www
Add user:
  $ solid-server -c config.json --log-level "" \
  > --add-user ${URL}toto --name "Toto Totoro" \
  > --cert-label "Toto's self-signed cert" \
  > --pem toto.pem --profile
Check directory:
  $ diff -r ref www
Re-Initialize directoties:
  $ rm -fr www
  $ cp -r origin www
Add user tutu:
  $ solid-server -c config.json --log-level "" \
  > --add-user ${URL}tutu \
  > --name "Tutu Turlututu" \
  > --cert-label "Tutu's self-signed cert" \
  > --create-cert tutu \
  > --profile
  Files created:
  tutu.pem: self-signed user certificate
  tutu.key: private key
  tutu.cfx: pkcs12 certificate to import in the browser
Run server, test client:
(use debug log-level to debug)
#for debug only: $ echo ${PWD}
  $ solid-server -c config.json --log-level "app" & sleep 1 ; \
  > test_user_creation.exe ${URL}tutu/Private
Kill server:
  $ killall solid-server || true
