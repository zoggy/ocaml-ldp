Initialize variables:
  $ export URL=https://localhost:9999/
  $ export PATH=${DUNE_SOURCEROOT}/_build/default/tests:${PATH}
  $ export BUILD_PATH_PREFIX_MAP=
Initialize directoties (must dereference links and change rights,
because the test must modify files in www):
  $ rm -fr www
  $ cp -Lr origin www
  $ chmod -R u+w www
Run server, test client:
(use debug log-level to debug)
#for debug only: $ echo ${PWD}
  $ solid-server -c config.json --log-level "app" & sleep 1 ; \
  > test_patch.exe ${URL}
Kill server:
  $ killall solid-server || true
Check directory:
  $ diff -r ref www
