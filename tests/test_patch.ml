(*********************************************************************************)
(*                OCaml-LDP                                                      *)
(*                                                                               *)
(*    Copyright (C) 2016-2024 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

let main =
  let%lwt h = Ldp_tls.make ~dbg:Lwt_io.(write_line stderr) () in
  let module H = (val h : Ldp.Http.Http) in

  let iri = Iri.of_string Sys.argv.(1) in
  let%lwt _ = H.patch_with_query iri
    ("PREFIX dc: <http://purl.org/dc/elements/1.1/>\n
   PREFIX cont: <"^(Iri.to_string iri)^">\n
   DELETE WHERE { cont: dc:title ?title };\n
   INSERT DATA { cont: dc:title \"New title\" }")
  in
  let iri_image = Iri.append_path iri ["image.png"] in
  let%lwt _ = H.patch_with_query iri_image
    ("PREFIX dc: <http://purl.org/dc/elements/1.1/>\n
   PREFIX image: <"^(Iri.to_string iri_image)^">\n
   INSERT DATA { image: dc:title \"Image title\" }")
  in

  let iri_graph = Iri.append_path iri ["graph.ttl"] in
  let%lwt _ = H.patch_with_query iri_graph
    ("PREFIX dc: <http://purl.org/dc/elements/1.1/>\n
   PREFIX g: <"^(Iri.to_string iri_graph)^">\n
   DELETE WHERE { g: dc:title ?title };\n
   INSERT DATA {
     g: dc:title \"New Graph title\";
        dc:title \"Nouveau title du graphe\"@fr
   }")
  in

  Lwt.return_unit

let () = Lwt_main.run main