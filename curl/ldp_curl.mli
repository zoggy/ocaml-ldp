(*********************************************************************************)
(*                OCaml-LDP                                                      *)
(*                                                                               *)
(*    Copyright (C) 2016-2024 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Performing queries using {!Curl}. *)

(** [make ~dbg ()] creates a new module to perform
   {{!Ldp.Http.module-type-Http}HTTP queries}.
   [dbg] is a debugging function.
  Optional arguments:
  {ul
   {- [cache_impl] indicates a cache implementation to use,}
   {- [cache] indicates a directory to use for cache, if no [cache_impl] is
     provided. If none is provided, no cache is used.}
   {- [cert] is a pair [(certificate file, key file)] to authenticate
     client in SSL. }
  }
*)
val make : ?cache_impl:(module Ldp.Http.Cache) ->
  ?cache_dir:string -> ?cert:(string*string) ->
  dbg:(string -> unit Lwt.t) -> unit -> (module Ldp.Http.Http) Lwt.t